#Importing libraries
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# Input data files are available in the "../input/" directory.
import os
import matplotlib.pyplot as plt#visualization
from PIL import  Image
import pandas as pd
import seaborn as sns#visualization
import itertools
import warnings
warnings.filterwarnings("ignore")
import io
import plotly.offline as py#visualization
# py.init_notebook_mode(connected=True)#visualization
import plotly.graph_objs as go#visualization
import plotly.tools as tls#visualization
import plotly.figure_factory as ff#visualization

telcom = pd.read_csv('../data/raw/WA_Fn-UseC_-Telco-Customer-Churn.csv')
print(telcom.head())


print("Rows     : ", telcom.shape[0])
print("Columns  : ", telcom.shape[1])
print("\nFeatures : \n", telcom.columns.tolist())
print("\nMissing values :  ", telcom.isnull().sum().values.sum())
print("\nUnique values :  \n", telcom.nunique())

# Data Manipulation

# Replacing spaces with null values in total charges column
telcom['TotalCharges'] = telcom["TotalCharges"].replace(" ", np.nan)

# Dropping null values from total charges column which contain .15% missing data
telcom = telcom[telcom["TotalCharges"].notnull()]
telcom = telcom.reset_index()[telcom.columns]

# convert to float type
telcom["TotalCharges"] = telcom["TotalCharges"].astype(float)

# replace 'No internet service' to No for the following columns
replace_cols = ['OnlineSecurity', 'OnlineBackup', 'DeviceProtection',
                'TechSupport', 'StreamingTV', 'StreamingMovies']
for i in replace_cols:
    telcom[i] = telcom[i].replace({'No internet service': 'No'})

# replace values
telcom["SeniorCitizen"] = telcom["SeniorCitizen"].replace({1: "Yes", 0: "No"})


# Tenure to categorical column
def tenure_lab(telcom):
    if telcom["tenure"] <= 12:
        return "Tenure_0-12"
    elif (telcom["tenure"] > 12) & (telcom["tenure"] <= 24):
        return "Tenure_12-24"
    elif (telcom["tenure"] > 24) & (telcom["tenure"] <= 48):
        return "Tenure_24-48"
    elif (telcom["tenure"] > 48) & (telcom["tenure"] <= 60):
        return "Tenure_48-60"
    elif telcom["tenure"] > 60:
        return "Tenure_gt_60"


telcom["tenure_group"] = telcom.apply(lambda telcom: tenure_lab(telcom),
                                      axis=1)

# Separating churn and non churn customers
churn = telcom[telcom["Churn"] == "Yes"]
not_churn = telcom[telcom["Churn"] == "No"]

# Separating catagorical and numerical columns
# Id_col = ['customerID']
# target_col = ["Churn"]
# cat_cols = telcom.nunique()[telcom.nunique() < 6].keys().tolist()
# cat_cols = [x for x in cat_cols if x not in target_col]
# num_cols = [x for x in telcom.columns if x not in cat_cols + target_col + Id_col]


from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler

#customer id col
Id_col = ['customerID']
#Target columns
target_col = ["Churn"]
#categorical columns
cat_cols = telcom.nunique()[telcom.nunique() < 6].keys().tolist()
# remove churn column
cat_cols = [x for x in cat_cols if x not in target_col]
# print("category columns \n",cat_cols)

#numerical columns
num_cols = [x for x in telcom.columns if x not in cat_cols + target_col + Id_col]
# print("numeric columns \n",num_cols)
#Binary columns with 2 values
bin_cols = telcom.nunique()[telcom.nunique() == 2].keys().tolist()
# print("binary columns \n",bin_cols)
#Columns more than 2 values
multi_cols = [i for i in cat_cols if i not in bin_cols]

#Label encoding binary categorical columns to 0,1 encoding
le = LabelEncoder()
# converts to binary values
for i in bin_cols:
    telcom[i] = le.fit_transform(telcom[i])


#Duplicating columns for multi value columns
telcom = pd.get_dummies(data=telcom, columns=multi_cols)

#Scaling Numerical columns
std = StandardScaler()
scaled = std.fit_transform(telcom[num_cols])
scaled = pd.DataFrame(scaled, columns=num_cols)

#dropping original values merging scaled values for numerical columns
df_telcom_og = telcom.copy()
telcom = telcom.drop(columns=num_cols, axis=1)
telcom = telcom.merge(scaled, left_index=True, right_index=True, how="left")

summary = (df_telcom_og[[i for i in df_telcom_og.columns if i not in Id_col]].
           describe().transpose().reset_index())

summary = summary.rename(columns={"index": "feature"})
summary = np.around(summary, 3)

val_lst = [summary['feature'], summary['count'],
           summary['mean'], summary['std'],
           summary['min'], summary['25%'],
           summary['50%'], summary['75%'], summary['max']]

col_labels = ['χαρακτ', 'πλήθος', 'μ.τ', 'τυπ.αποκλ.', 'ελαχ', '25%', '50%', '75%', 'μέγιστο']
trace = go.Table(header=dict(values=col_labels, # summary.columns.tolist()
                               line=dict(color=['#506784']),
                               fill=dict(color=['#119DFF']),
                               ),
                 cells=dict(values=val_lst,
                            line=dict(color=['#506784']),
                            fill=dict(color=["lightgrey", '#F5F8FF'])
                               ),
                 columnwidth=[200, 60, 100, 100, 60, 60, 80, 80, 80])
layout = go.Layout(dict(title="Αναφορά Χαρακτηριστικών"))
figure = go.Figure(data=[trace], layout=layout)
py.plot(figure)


# function  for pie plot for customer attrition types
def plot_pie(column):
    trace1 = go.Pie(values=churn[column].value_counts().values.tolist(),
                    labels=churn[column].value_counts().keys().tolist(),
                    hoverinfo="label+percent+name",
                    domain=dict(x=[0, .48]),
                    name="Churn Customers",
                    marker=dict(line=dict(width=2,
                                          color="rgb(243,243,243)")
                                ),
                    hole=.6
                    )
    trace2 = go.Pie(values=not_churn[column].value_counts().values.tolist(),
                    labels=not_churn[column].value_counts().keys().tolist(),
                    hoverinfo="label+percent+name",
                    marker=dict(line=dict(width=2,
                                          color="rgb(243,243,243)")
                                ),
                    domain=dict(x=[.52, 1]),
                    hole=.6,
                    name="Non churn customers"
                    )

    layout = go.Layout(dict(title=column + " distribution in customer attrition ",
                            plot_bgcolor="rgb(243,243,243)",
                            paper_bgcolor="rgb(243,243,243)",
                            annotations=[dict(text="churn customers",
                                              font=dict(size=13),
                                              showarrow=False,
                                              x=.15, y=.5),
                                         dict(text="Non churn customers",
                                              font=dict(size=13),
                                              showarrow=False,
                                              x=.88, y=.5
                                              )
                                         ]
                            )
                       )
    data = [trace1, trace2]
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename=column)


# function  for histogram for customer attrition types
def histogram(column):
    trace1 = go.Histogram(x=churn[column],
                          histnorm="percent",
                          name="Churn Customers",
                          marker=dict(line=dict(width=.5,
                                                color="black"
                                                )
                                      ),
                          opacity=.9
                          )

    trace2 = go.Histogram(x=not_churn[column],
                          histnorm="percent",
                          name="Non churn customers",
                          marker=dict(line=dict(width=.5,
                                                color="black"
                                                )
                                      ),
                          opacity=.9
                          )

    data = [trace1, trace2]
    layout = go.Layout(dict(title=column + " distribution in customer attrition ",
                            plot_bgcolor="rgb(243,243,243)",
                            paper_bgcolor="rgb(243,243,243)",
                            xaxis=dict(gridcolor='rgb(255, 255, 255)',
                                       title=column,
                                       zerolinewidth=1,
                                       ticklen=5,
                                       gridwidth=2
                                       ),
                            yaxis=dict(gridcolor='rgb(255, 255, 255)',
                                       title="percent",
                                       zerolinewidth=1,
                                       ticklen=5,
                                       gridwidth=2
                                       ),
                            )
                       )
    fig = go.Figure(data=data, layout=layout)

    py.plot(fig, filename=column+"histo")


# function  for scatter plot matrix  for numerical columns in data
def scatter_matrix(df):
    df = df.sort_values(by="Churn", ascending=True)
    classes = df["Churn"].unique().tolist()
    classes

    class_code = {classes[k]: k for k in range(2)}
    class_code

    color_vals = [class_code[cl] for cl in df["Churn"]]
    color_vals

    pl_colorscale = "Portland"

    pl_colorscale

    text = [df.loc[k, "Churn"] for k in range(len(df))]
    text

    trace = go.Splom(dimensions=[dict(label="tenure",
                                      values=df["tenure"]),
                                 dict(label='MonthlyCharges',
                                      values=df['MonthlyCharges']),
                                 dict(label='TotalCharges',
                                      values=df['TotalCharges'])],
                     text=text,
                     marker=dict(color=color_vals,
                                 colorscale=pl_colorscale,
                                 size=3,
                                 showscale=False,
                                 line=dict(width=.1,
                                           color='rgb(230,230,230)'
                                           )
                                 )
                     )
    axis = dict(showline=True,
                zeroline=False,
                gridcolor="#fff",
                ticklen=4
                )

    layout = go.Layout(dict(title=
                            "Scatter plot matrix for Numerical columns for customer attrition",
                            autosize=False,
                            height=800,
                            width=800,
                            dragmode="select",
                            hovermode="closest",
                            plot_bgcolor='rgba(240,240,240, 0.95)',
                            xaxis1=dict(axis),
                            yaxis1=dict(axis),
                            xaxis2=dict(axis),
                            yaxis2=dict(axis),
                            xaxis3=dict(axis),
                            yaxis3=dict(axis),
                            )
                       )
    data = [trace]
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig)


# # for all categorical columns plot pie
# for i in cat_cols:
#     plot_pie(i)
#
# # for all categorical columns plot histogram
# for i in num_cols:
#     histogram(i)

# scatter plot matrix
# scatter_matrix(telcom)

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.metrics import roc_auc_score, roc_curve, scorer
from sklearn.metrics import f1_score
import statsmodels.api as sm
from sklearn.metrics import precision_score, recall_score
from yellowbrick.classifier import DiscriminationThreshold

# splitting train and test data
train, test = train_test_split(telcom, test_size=.25, random_state=111)

##seperating dependent and independent variables
cols = [i for i in telcom.columns if i not in Id_col + target_col]
train_X = train[cols]
train_Y = train[target_col]
test_X = test[cols]
test_Y = test[target_col]


# Function attributes
# dataframe     - processed dataframe
# Algorithm     - Algorithm used
# training_x    - predictor variables dataframe(training)
# testing_x     - predictor variables dataframe(testing)
# training_y    - target variable(training)
# training_y    - target variable(testing)
# cf - ["coefficients","features"](cooefficients for logistic
# regression,features for tree based models)
# threshold_plot - if True returns threshold plot for model
def telecom_churn_prediction(algorithm, training_x, testing_x,
                             training_y, testing_y, cols, cf, threshold_plot, show_chart=False):
    # model
    algorithm.fit(training_x, training_y)
    predictions = algorithm.predict(testing_x)
    probabilities = algorithm.predict_proba(testing_x)
    # coeffs
    if cf == "coefficients":
        coefficients = pd.DataFrame(algorithm.coef_.ravel())
    elif cf == "features":
        coefficients = pd.DataFrame(algorithm.feature_importances_)

    column_df = pd.DataFrame(cols)
    coef_sumry = (pd.merge(coefficients, column_df, left_index=True,
                           right_index=True, how="left"))
    coef_sumry.columns = ["coefficients", "features"]
    coef_sumry = coef_sumry.sort_values(by="coefficients", ascending=False)

    print(algorithm)
    print("\n Classification report : \n", classification_report(testing_y, predictions))
    print("Accuracy   Score : ", accuracy_score(testing_y, predictions))
    # confusion matrix
    conf_matrix = confusion_matrix(testing_y, predictions)
    print(conf_matrix)
    # roc_auc_score
    model_roc_auc = roc_auc_score(testing_y, predictions)
    print("Area under curve : ", model_roc_auc, "\n")
    fpr, tpr, thresholds = roc_curve(testing_y, probabilities[:, 1])

    if show_chart:
        # plot confusion matrix
        trace1 = go.Heatmap(z=conf_matrix,
                            x=["Not churn", "Churn"],
                            y=["Not churn", "Churn"],
                            showscale=False, colorscale="Picnic",
                            name="matrix")

        # plot roc curve
        trace2 = go.Scatter(x=fpr, y=tpr,
                            name="Roc : " + str(model_roc_auc),
                            line=dict(color=('rgb(22, 96, 167)'), width=2))
        trace3 = go.Scatter(x=[0, 1], y=[0, 1],
                            line=dict(color=('rgb(205, 12, 24)'), width=2,
                                      dash='dot'))

        # plot coeffs
        trace4 = go.Bar(x=coef_sumry["features"], y=coef_sumry["coefficients"],
                        name="coefficients",
                        marker=dict(color=coef_sumry["coefficients"],
                                    colorscale="Picnic",
                                    line=dict(width=.6, color="black")))

        # subplots
        fig = tls.make_subplots(rows=2, cols=2, specs=[[{}, {}], [{'colspan': 2}, None]],
                                subplot_titles=('Confusion Matrix',
                                                'Receiver operating characteristic',
                                                'Feature Importances'))

        fig.append_trace(trace1, 1, 1)
        fig.append_trace(trace2, 1, 2)
        fig.append_trace(trace3, 1, 2)
        fig.append_trace(trace4, 2, 1)

        fig['layout'].update(showlegend=False, title="Model performance",
                             autosize=False, height=900, width=800,
                             plot_bgcolor='rgba(240,240,240, 0.95)',
                             paper_bgcolor='rgba(240,240,240, 0.95)',
                             margin=dict(b=195))
        fig["layout"]["xaxis2"].update(dict(title="false positive rate"))
        fig["layout"]["yaxis2"].update(dict(title="true positive rate"))
        fig["layout"]["xaxis3"].update(dict(showgrid=True, tickfont=dict(size=10),
                                            tickangle=90))
        py.plot(fig)

        if threshold_plot:
            visualizer = DiscriminationThreshold(algorithm)
            visualizer.fit(training_x, training_y)
            visualizer.poof()


logit = LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,
                           intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,
                           penalty='l2', random_state=None, solver='liblinear', tol=0.0001,
                           verbose=0, warm_start=False)

telecom_churn_prediction(logit, train_X, test_X, train_Y, test_Y, cols, "coefficients", threshold_plot=True,
                         show_chart=False)

from imblearn.over_sampling import SMOTE

cols = [i for i in telcom.columns if i not in Id_col+target_col]

smote_X = telcom[cols]
smote_Y = telcom[target_col]

#Split train and test data
smote_train_X, smote_test_X, smote_train_Y, smote_test_Y = train_test_split(smote_X, smote_Y,
                                                                         test_size=.25,
                                                                         random_state=111)

#oversampling minority class using smote
os = SMOTE(random_state=0)
os_smote_X,os_smote_Y = os.fit_sample(smote_train_X, smote_train_Y)
os_smote_X = pd.DataFrame(data=os_smote_X, columns=cols)
os_smote_Y = pd.DataFrame(data=os_smote_Y, columns=target_col)
###

logit_smote = LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,
          intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,
          penalty='l2', random_state=None, solver='liblinear', tol=0.0001,
          verbose=0, warm_start=False)

telecom_churn_prediction(logit_smote, os_smote_X, test_X, os_smote_Y, test_Y,
                         cols, "coefficients", threshold_plot=True, show_chart=False)

from sklearn.metrics import f1_score
from sklearn.metrics import cohen_kappa_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB(priors=None)
from sklearn.svm import SVC
from xgboost import XGBClassifier


# gives model report in dataframe
def model_report(model, training_x, testing_x, training_y, testing_y, name):
    model.fit(training_x, training_y)
    predictions = model.predict(testing_x)
    accuracy = accuracy_score(testing_y, predictions)
    recallscore = recall_score(testing_y, predictions)
    precision = precision_score(testing_y, predictions)
    roc_auc = roc_auc_score(testing_y, predictions)
    f1score = f1_score(testing_y, predictions)
    kappa_metric = cohen_kappa_score(testing_y, predictions)

    df = pd.DataFrame({"Model": [name],
                       "Accuracy_score": [accuracy],
                       "Recall_score": [recallscore],
                       "Precision": [precision],
                       "f1_score": [f1score],
                       "Area_under_curve": [roc_auc],
                       "Kappa_metric": [kappa_metric],
                       })
    return df



# outputs for every model
model1 = model_report(logit, train_X, test_X, train_Y, test_Y,
                      "Logistic Regression(Baseline_model)")
model2 = model_report(logit_smote, os_smote_X, test_X, os_smote_Y, test_Y,
                      "Logistic Regression(SMOTE)")
decision_tree = DecisionTreeClassifier(max_depth = 9,
                                       random_state = 123,
                                       splitter  = "best",
                                       criterion = "gini",
                                      )
model3 = model_report(decision_tree, train_X, test_X, train_Y, test_Y,
                      "Decision Tree")
knn = KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
           metric_params=None, n_jobs=1, n_neighbors=5, p=2,
           weights='uniform')
model4 = model_report(knn, os_smote_X, test_X, os_smote_Y, test_Y,
                      "KNN Classifier")
rfc = RandomForestClassifier(n_estimators=1000,
                             random_state=123,
                             max_depth=9,
                             criterion="gini")
model5 = model_report(rfc, train_X, test_X, train_Y, test_Y,
                      "Random Forest Classifier")
model6 = model_report(gnb, os_smote_X, test_X, os_smote_Y, test_Y,
                      "Naive Bayes")
#Support vector classifier
#using linear hyper plane
svc_lin = SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
               decision_function_shape='ovr', degree=3, gamma=1.0, kernel='linear',
               max_iter=-1, probability=True, random_state=None, shrinking=True,
               tol=0.001, verbose=False)
model7 = model_report(svc_lin, os_smote_X, test_X, os_smote_Y, test_Y,
                      "SVM Classifier Linear")
xgc = XGBClassifier(base_score=0.5, booster='gbtree', colsample_bylevel=1,
                    colsample_bytree=1, gamma=0, learning_rate=0.9, max_delta_step=0,
                    max_depth = 7, min_child_weight=1, missing=None, n_estimators=100,
                    n_jobs=1, nthread=None, objective='binary:logistic', random_state=0,
                    reg_alpha=0, reg_lambda=1, scale_pos_weight=1, seed=None,
                    silent=True, subsample=1)

model8 = model_report(xgc, os_smote_X, test_X, os_smote_Y, test_Y,
                       "XGBoost Classifier")

# concat all models
model_performances = pd.concat([model1, model2, model3,
                                model4, model5, model6,
                                model7, model8], axis=0).reset_index()

model_performances = model_performances.drop(columns="index", axis=1)

table = ff.create_table(np.round(model_performances, 4))

py.plot(table)

lst = [logit, logit_smote, decision_tree, knn, rfc,
       gnb, svc_lin, xgc]

length = len(lst)

mods = ['Logistic Regression(Baseline_model)', 'Logistic Regression(SMOTE)',
        'Decision Tree', 'KNN Classifier', 'Random Forest Classifier', 'Naive Bayes',
        'SVM Classifier Linear', 'XGBoost Classifier']

fig = plt.figure(figsize=(13,15))
fig.set_facecolor("#F3F3F3")
for i, j, k in itertools.zip_longest(lst, range(length), mods):
    plt.subplot(4, 3, j+1)
    predictions = i.predict(test_X)
    conf_matrix = confusion_matrix(predictions,test_Y)
    sns.heatmap(conf_matrix, annot=True, fmt = "d", square=True,
                xticklabels=["not churn", "churn"],
                yticklabels=["not churn", "churn"],
                linewidths = 2,linecolor="w", cmap="Set1")
    plt.title(k, color="b")
    plt.subplots_adjust(wspace=.3, hspace=.3)

plt.show()

