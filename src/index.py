import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

import matplotlib
# matplotlib.use("agg")
# matplotlib.use('TkAgg')
import matplotlib.ticker as mtick # For specifying the axes tick format
import matplotlib.pyplot as plt
# %matplotlib inline
import seaborn as sns
import keras
from keras import Sequential
from keras.layers import Dense
from keras.models import load_model


df = pd.read_csv('../data/raw/WA_Fn-UseC_-Telco-Customer-Churn.csv')
print(df.head())

# check the unique values to see the range of the values per feature
# for item in df.columns:
#     print(item)
#     print(df[item].unique())


def prepare_data_simple(data_frame):
    columns_to_convert = ['Partner',
                          'Dependents',
                          'PhoneService',
                          'PaperlessBilling',
                          'Churn']

    for item in columns_to_convert:
        data_frame[item].replace(["Yes", "No"], [1, 0], inplace=True)
        # data_frame[item].replace(to_replace='no', value=0, inplace=True)
    # change the total charges from string to float, taking into account empty strings
    data_frame.TotalCharges.replace([" "], ["0"], inplace=True)
    data_frame.TotalCharges = data_frame.TotalCharges.astype(float)
    # create two datasets
    churners_number = len(data_frame[data_frame['Churn'] == 1])
    print("Number of churners", churners_number)
    churners = (data_frame[data_frame['Churn'] == 1])
    non_churners = data_frame[data_frame['Churn'] == 0].sample(n=churners_number)
    print("Number of non-churners", len(non_churners))
    data_frame2 = churners.append(non_churners)

    return data_frame, data_frame2


def show_correlations(dataframe, show_chart=True):
    fig = plt.figure(figsize=(20, 10))
    corr = dataframe.corr()
    if show_chart:
        sns.heatmap(corr,
                    xticklabels=corr.columns.values,
                    yticklabels=corr.columns.values,
                    annot=True)
    # plt.show()
    return corr


def prepare_data_for_tensor_flow(data_frame):
    """Prepare the data for the model"""
    # change values from 0-1 to NO, YES
    data_frame.SeniorCitizen.replace([0, 1], ["No", "Yes"], inplace=True)
    # change the total charges from string to float, taking into account empty strings
    data_frame.TotalCharges.replace([" "], ["0"], inplace=True)
    data_frame.TotalCharges = data_frame.TotalCharges.astype(float)
    data_frame.drop("customerID", axis=1, inplace=True)
    # to create a binary classification model
    data_frame.Churn.replace(["Yes", "No"], [1, 0], inplace=True)
    # One-hot encoding
    data_frame = pd.get_dummies(data_frame)
    return data_frame


def run_model_tensor_flow(X, y):
    # Split train and test data
    smote_train_X, smote_test_X, smote_train_Y, smote_test_Y = train_test_split(X, y,
                                                                                test_size=.25,
                                                                                random_state=111)

    # oversampling minority class using smote
    os = SMOTE(random_state=0)
    os_smote_X, os_smote_Y = os.fit_sample(smote_train_X, smote_train_Y)
    os_smote_X = pd.DataFrame(data=os_smote_X, columns=cols)
    os_smote_Y = pd.DataFrame(data=os_smote_Y, columns=target_col)
    ###
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1234)

    model = Sequential()
    model.add(Dense(16, input_dim=smote_train_X.shape[1], activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    # callbacks = [keras.callbacks.EarlyStopping(
    #     monitor='val_loss', patience=2)]

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', 'binary_crossentropy'])
    history = model.fit(os_smote_X, os_smote_Y, epochs=150, batch_size=10,
                        validation_data=(smote_test_X, smote_test_Y),
                        verbose=2)
    _, accuracy, binary_crossentropy = model.evaluate(smote_test_X, smote_test_Y)
    print('test accurancy:', accuracy)
    history_data = history.history
    print('Validation accuracy: {acc}, loss: {loss}'.format(
        acc=history_data['acc'][-1], loss=history_data['loss'][-1]))
    plot_history(history)
    model.save('my_model.h5')


def plot_history(history, key='binary_crossentropy'):
    plt.figure(figsize=(16, 10))
    val = plt.plot(history.epoch, history.history['val_'+key],
                   '--', label='Baseline Val')
    plt.plot(history.epoch, history.history[key], color=val[0].get_color(),
             label='Baseline Train')
    plt.xlabel('Epochs')
    plt.ylabel(key.replace('_', ' ').title())
    plt.legend()
    plt.xlim([0, max(history.epoch)])


"""
df1, df2 = prepare_data_simple(df)
# print("DF1 TEST \n",df1.head())

# plt.figure(figsize=(15,8))
# df2.corr()['Churn'].sort_values(ascending=False).plot(kind='bar')
# plt.show()
correlation_df = show_correlations(df2, show_chart=True)
# plt.show()
# print(correlation_df)

try:
    customer_id = df2['customerID'] # Store this as customer_id variable
    del df2['customerID'] # Don't need in ML DF
except:
    print("already removed customerID")

ml_dummies = pd.get_dummies(df2)
ml_dummies.fillna(value=0, inplace=True)
ml_dummies['---randomColumn---'] = np.random.randint(0,1000, size=len(ml_dummies))

# display correlation between churn and other features
print(show_correlations(ml_dummies, show_chart=True)["Churn"].sort_values(ascending=False))
try:
    label = ml_dummies['Churn'] # Remove the label before training the model
    del ml_dummies['Churn']
except:
    print("label already removed.")

feature_train, feature_test, label_train, label_test = train_test_split(ml_dummies, label, test_size=0.3)

# Classifiers
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn import svm

# clf = svm.SVC(kernel='linear', C=10).fit(feature_train, label_train)
# score = clf.score(feature_test, label_test)
# print("SVM")
# print(score)

classifiers = [
    KNeighborsClassifier(5),
    DecisionTreeClassifier(max_depth=5),
    # SGDClassifier(loss="hinge", penalty="l2", max_iter=5),
    # svm.SVC(gamma='scale', random_state=0)
]

# iterate over classifiers
for item in classifiers:
    classifier_name = ((str(item)[:(str(item).find("("))]))
    print(classifier_name)

    # Create classifier, train it and test it.
    clf = item
    clf.fit(feature_train, label_train)
    pred = clf.predict(feature_test)
    score = clf.score(feature_test, label_test)
    print(round(score, 3), "\n", "- - - - - ", "\n")
    from sklearn.metrics import classification_report
    class_names = ['Not churned','churned']
    eval_metrics = classification_report(label_test, pred, target_names=class_names)
    print(eval_metrics)

feature_df = pd.DataFrame()
feature_df['features'] = ml_dummies.columns
feature_df['importance'] = clf.feature_importances_
feature_df.sort_values(by='importance', ascending=False)
feature_df.set_index(keys='features').sort_values(by='importance', ascending=True).plot(kind='barh', figsize=(20, 15))

activate = output_df[output_df['churn'] == 0]
activate[['customerID','churn','prediction']]
# plt.show()
"""
df = prepare_data_for_tensor_flow(df)
target_col = ["Churn"]

from imblearn.over_sampling import SMOTE

cols = [i for i in df.columns if i not in target_col]

smote_X = df[cols]
smote_Y = df[target_col]

# X = df[cols]
# y = df.Churn
run_model_tensor_flow(smote_X, smote_Y)
plt.show()



